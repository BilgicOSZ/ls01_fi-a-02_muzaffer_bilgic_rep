//package eingabeAusgabe;

import java.awt.print.Printable;
import java.math.BigDecimal;

import javafx.scene.control.DialogEvent;

/** Variablen.java
    Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
    @author
    @version
*/
public class Variablen {
  public static void main(String [] args){
    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
          Vereinbaren Sie eine geeignete Variable */
	 int z�hlerProDurchlauf;
	 

    /* 2. Weisen Sie dem Zaehler den Wert 25 zu
          und geben Sie ihn auf dem Bildschirm aus.*/
	    z�hlerProDurchlauf = 25;
	    System.out.println("Der aktuelle Durchlauf : " + z�hlerProDurchlauf);
	  

    /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
          eines Programms ausgewaehlt werden.
          Vereinbaren Sie eine geeignete Variable */
	    char menuepunkt;

    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
          und geben Sie ihn auf dem Bildschirm aus.*/
	    menuepunkt = 'C';
	    System.out.println("Men�punkt kann aufgerufen werden mit der Taste: "+ menuepunkt);

    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
          notwendig.
          Vereinbaren Sie eine geeignete Variable */
	    long bigNumber;

    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
          und geben Sie sie auf dem Bildschirm aus.*/
	    bigNumber = 299792L;
	    System.out.println("Die Lichtgeschwindigkeit betr�gt: "+bigNumber +" km/s");

    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
          soll die Anzahl der Mitglieder erfasst werden.
          Vereinbaren Sie eine geeignete Variable und initialisieren sie
          diese sinnvoll.*/
	    int anzahlMitglieder = 820;

    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
	    System.out.println("Die Anzahl der Mitglieder betr�gt: " +anzahlMitglieder +" Personen");

    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
          dem Bildschirm aus.*/
	    double elementarLadung = -0.0000000000000000001602; //Oder direkt "1,602E-19" schreiben, double erkennt dies
	    System.out.println("Die Elementarladung betr�gt: "+ elementarLadung);

    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
          Vereinbaren Sie eine geeignete Variable. */
	    boolean zahlungErfolgt;

    /*11. Die Zahlung ist erfolgt.
          Weisen Sie der Variable den entsprechenden Wert zu
          und geben Sie die Variable auf dem Bildschirm aus.*/
	    zahlungErfolgt = true;
	    System.out.println("Zahlung ist erfolgt?: "+ zahlungErfolgt);

  }//main
}// Variablen
