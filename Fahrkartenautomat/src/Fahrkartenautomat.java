﻿import java.security.DrbgParameters.NextBytes;
import java.util.Scanner;

public class Fahrkartenautomat {

	public static void main(String[] args) {

		boolean abfrage = true;

		while (abfrage = true) {
			double zuZahlen = fahrkartenbestellungErfassen();
			double rückgabebetrag = fahrkartenBezahlen(zuZahlen);

			warte(2000);
			rueckgeldAusgeben(rückgabebetrag);

			abschied();
			System.out.println("nächster Kunde?   (j/n)");
			Scanner abfragen = new Scanner(System.in);
			char antwortString = abfragen.next().charAt(0);
			if (antwortString == 'n') {

			System.out.println("Tschüss!");
			
				break;
			}
		}

	}

	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Zu zahlender Betrag (EURO): ");
		double zuZahlenderBetrag = tastatur.nextDouble();
		System.out.print("Anzahl der Tickets?: ");
		int ticketanzahl = tastatur.nextInt();
		double zuZahlen = zuZahlenderBetrag * ticketanzahl;
		return zuZahlen;
	}

	public static double fahrkartenBezahlen(double zuZahlen) {
		Scanner tastatur = new Scanner(System.in);
		double eingeworfeneMünze;
		double eingezahlterGesamtbetrag = 0.00D;
		double rückgabebetrag;

		while (eingezahlterGesamtbetrag < zuZahlen) {
			System.out.printf("Noch zu zahlen: %.2f%s\n ", (zuZahlen - eingezahlterGesamtbetrag), " EURO");

			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;

		}
		rückgabebetrag = eingezahlterGesamtbetrag - zuZahlen;
		return rückgabebetrag;
	}

	public static void warte(int millisekunde) {

		try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
		}
	}

	

	

	public static void rueckgeldAusgeben(double rückgabebetrag) {

		// System.out.println(rückgabebetrag); Rundungstest

		rückgabebetrag = Math.round(100.0 * rückgabebetrag);
		rückgabebetrag = rückgabebetrag / 100;

		if (rückgabebetrag > 0.0) {
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f%s ", rückgabebetrag, " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rückgabebetrag -= 2.0;
				rückgabebetrag = Math.round(100.0 * rückgabebetrag);
				rückgabebetrag = rückgabebetrag / 100;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rückgabebetrag -= 1.0;
				rückgabebetrag = Math.round(100.0 * rückgabebetrag);
				rückgabebetrag = rückgabebetrag / 100;

			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rückgabebetrag -= 0.5;
				rückgabebetrag = Math.round(100.0 * rückgabebetrag);
				rückgabebetrag = rückgabebetrag / 100;

			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rückgabebetrag -= 0.2;
				rückgabebetrag = Math.round(100.0 * rückgabebetrag);
				rückgabebetrag = rückgabebetrag / 100;

			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rückgabebetrag -= 0.1;
				rückgabebetrag = Math.round(100.0 * rückgabebetrag);
				rückgabebetrag = rückgabebetrag / 100;

			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rückgabebetrag -= 0.05;
				rückgabebetrag = Math.round(100.0 * rückgabebetrag);
				rückgabebetrag = rückgabebetrag / 100;

			}
		}

	}

	public static void abschied() {
		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
	}
}
